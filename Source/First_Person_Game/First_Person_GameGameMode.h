// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "First_Person_GameGameMode.generated.h"

UCLASS(minimalapi)
class AFirst_Person_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFirst_Person_GameGameMode();
};



