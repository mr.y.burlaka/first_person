// Copyright Epic Games, Inc. All Rights Reserved.

#include "First_Person_GameGameMode.h"
#include "First_Person_GameHUD.h"
#include "First_Person_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFirst_Person_GameGameMode::AFirst_Person_GameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFirst_Person_GameHUD::StaticClass();
}
