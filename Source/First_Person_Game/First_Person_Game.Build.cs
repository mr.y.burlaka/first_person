// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class First_Person_Game : ModuleRules
{
	public First_Person_Game(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
